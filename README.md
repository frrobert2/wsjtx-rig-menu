# WSJTX Rig Menu


## Description
It is a simple YAD Menu for pulling up WSJTX 

## Installation
Download the file, edit it to meet your needs, save it to your path, and make it executable.  

## Usage
I call it by hitting alt+F2 and typing in the mame.

## Aknowledgment
This menu was inspired by the video,  How to run multiple WSJT-X on one computer, FT8 under different callsigns and separate QSO logs, https://www.youtube.com/watch?v=_a1iay7sBTM and by Pat Menu by KM4ACK

## License
GPL3
